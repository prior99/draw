import {
    RemoteBaseGameState,
    GameStateType,
    RemoteGameStateRoundEndScore,
    DrawingInstructions,
    RemoteGameStateResolving,
    RemoteGameStateGuessingDone,
    GuessingInstructions,
    ImageInfo,
    RemoteGameStateGuessing,
    RemoteGameStateSuggestingDone,
    RemoteGameStateSuggesting,
    RemoteGameStateWaitingForOtherPlayersImages,
    RemoteGameStateWaitingForImages,
    RemoteGameStateDrawingDone,
    RemoteGameStateDrawing,
    RemoteGameStateStarted,
    RemoteGameStateLobby,
    SuggestingInstructions,
    WordInfo,
} from "../types";

export abstract class LocalBaseGameState {
    public abstract state: GameStateType;
    public abstract toRemote(): RemoteBaseGameState;
}

export class LocalGameStateLobby extends LocalBaseGameState implements RemoteGameStateLobby {
    public state = GameStateType.LOBBY as const;

    public startGame(): LocalGameStateStarted {
        return new LocalGameStateStarted(this);
    }

    public toRemote(): RemoteGameStateLobby {
        const { state } = this;
        return { state };
    }
}

export class LocalGameStateStarted extends LocalBaseGameState implements RemoteGameStateStarted {
    public state = GameStateType.STARTED as const;

    constructor(_last: LocalGameStateLobby) {
        super();
    }

    public startDrawing(instructions: DrawingInstructions): LocalGameStateDrawing {
        return new LocalGameStateDrawing(this, instructions);
    }

    public toRemote(): RemoteGameStateStarted {
        const { state } = this;
        return { state };
    }
}

export class LocalGameStateDrawing extends LocalBaseGameState implements RemoteGameStateDrawing {
    public state = GameStateType.DRAWING as const;
    public instructions: DrawingInstructions;

    constructor(_last: LocalGameStateStarted | LocalGameStateRoundEndScore, instructions: DrawingInstructions) {
        super();
        this.instructions = instructions;
    }

    public drawingDone(pickedWord: WordInfo, data: string): LocalGameStateDrawingDone {
        return new LocalGameStateDrawingDone(this, pickedWord, data);
    }

    public timeOver(): LocalGameStateWaitingForImages {
        return new LocalGameStateWaitingForImages(this);
    }

    public toRemote(): RemoteGameStateDrawing {
        const { state, instructions } = this;
        return { state, instructions };
    }
}

export class LocalGameStateDrawingDone extends LocalBaseGameState implements RemoteGameStateDrawingDone {
    public state = GameStateType.DRAWING_DONE as const;
    public instructions: DrawingInstructions;
    public pickedWord: WordInfo;
    public data: string;

    constructor(last: LocalGameStateDrawing, pickedWord: WordInfo, data: string) {
        super();
        this.instructions = last.instructions;
        this.pickedWord = pickedWord;
        this.data = data;
    }

    public timeOver(): LocalGameStateWaitingForImages {
        return new LocalGameStateWaitingForImages(this);
    }

    public toRemote(): RemoteGameStateDrawingDone {
        const { state, instructions, data, pickedWord } = this;
        return { state, instructions, data, pickedWord };
    }
}

export class LocalGameStateWaitingForImages extends LocalBaseGameState implements RemoteGameStateWaitingForImages {
    public state = GameStateType.WAITING_FOR_IMAGES as const;

    constructor(_last: LocalGameStateDrawing | LocalGameStateDrawingDone) {
        super();
    }

    public images(images: Map<string, ImageInfo>): LocalGameStateWaitingForOtherPlayersImages {
        return new LocalGameStateWaitingForOtherPlayersImages(this, images);
    }

    public toRemote(): RemoteGameStateWaitingForImages {
        const { state } = this;
        return { state };
    }
}

export class LocalGameStateWaitingForOtherPlayersImages extends LocalBaseGameState
    implements RemoteGameStateWaitingForOtherPlayersImages {
    public state = GameStateType.WAITING_FOR_OTHER_PLAYERS_IMAGES as const;
    public images: Map<string, ImageInfo>;

    constructor(last: LocalGameStateWaitingForImages, images: Map<string, ImageInfo>) {
        super();
        this.images = images;
    }

    public allPlayersDoneLoading(instructions: SuggestingInstructions): LocalGameStateSuggesting {
        return new LocalGameStateSuggesting(this, instructions);
    }

    public toRemote(): RemoteGameStateWaitingForOtherPlayersImages {
        const { state } = this;
        return { state };
    }
}

export class LocalGameStateSuggesting extends LocalBaseGameState implements RemoteGameStateSuggesting {
    public state = GameStateType.SUGGESTING as const;
    public images: Map<string, ImageInfo>;
    public instructions: SuggestingInstructions;

    constructor(last: LocalGameStateWaitingForOtherPlayersImages, instructions: SuggestingInstructions) {
        super();
        this.images = last.images;
        this.instructions = instructions;
    }

    public suggestingDone(suggestion: string): LocalGameStateSuggestingDone {
        return new LocalGameStateSuggestingDone(this, suggestion);
    }

    public timeOver(instructions: GuessingInstructions): LocalGameStateGuessing {
        return new LocalGameStateGuessing(this, instructions);
    }

    public toRemote(): RemoteGameStateSuggesting {
        const { state } = this;
        return { state };
    }
}

export class LocalGameStateSuggestingDone extends LocalBaseGameState implements RemoteGameStateSuggestingDone {
    public state = GameStateType.SUGGESTING_DONE as const;
    public instructions: SuggestingInstructions;
    public suggestion: string;
    public images: Map<string, ImageInfo>;

    constructor(last: LocalGameStateSuggesting, suggestion: string) {
        super();
        this.images = last.images;
        this.instructions = last.instructions;
        this.suggestion = suggestion;
    }

    public timeOver(instructions: GuessingInstructions): LocalGameStateGuessing {
        return new LocalGameStateGuessing(this, instructions);
    }

    public toRemote(): RemoteGameStateSuggestingDone {
        const { state, suggestion } = this;
        return { state, suggestion };
    }
}

export class LocalGameStateGuessing extends LocalBaseGameState implements RemoteGameStateGuessing {
    public state = GameStateType.GUESSING as const;
    public images: Map<string, ImageInfo>;
    public instructions: GuessingInstructions;

    constructor(last: LocalGameStateSuggesting | LocalGameStateSuggestingDone, instructions: GuessingInstructions) {
        super();
        this.images = last.images;
        this.instructions = instructions;
    }

    public guessingDone(pickedOption: string): LocalGameStateGuessingDone {
        return new LocalGameStateGuessingDone(this, pickedOption);
    }

    public timeOver(): LocalGameStateResolving {
        return new LocalGameStateResolving(this);
    }

    public toRemote(): RemoteGameStateGuessing {
        const { state } = this;
        return { state };
    }
}

export class LocalGameStateGuessingDone extends LocalBaseGameState implements RemoteGameStateGuessingDone {
    public state = GameStateType.GUESSING_DONE as const;
    public images: Map<string, ImageInfo>;
    public instructions: GuessingInstructions;
    public pickedOption: string;

    constructor(last: LocalGameStateGuessing, pickedOption: string) {
        super();
        this.instructions = last.instructions;
        this.images = last.images;
        this.pickedOption = pickedOption;
    }

    public timeOver(): LocalGameStateResolving {
        return new LocalGameStateResolving(this);
    }

    public toRemote(): RemoteGameStateGuessingDone {
        const { state, pickedOption } = this;
        return { state, pickedOption };
    }
}

export class LocalGameStateResolving extends LocalBaseGameState implements RemoteGameStateResolving {
    public state = GameStateType.RESOLVING as const;
    public images: Map<string, ImageInfo>;
    public instructions: GuessingInstructions;
    public pickedOption: string | undefined;

    constructor(last: LocalGameStateGuessing | LocalGameStateGuessingDone) {
        super();
        this.images = last.images;
        this.instructions = last.instructions;
        this.pickedOption = last instanceof LocalGameStateGuessingDone ? last.pickedOption : undefined;
    }

    public timeOver(scores: Map<string, number>): LocalGameStateRoundEndScore {
        return new LocalGameStateRoundEndScore(this, scores);
    }

    public toRemote(): RemoteGameStateResolving {
        const { state, pickedOption } = this;
        return { state, pickedOption };
    }
}

export class LocalGameStateRoundEndScore extends LocalBaseGameState implements RemoteGameStateRoundEndScore {
    public state = GameStateType.ROUND_END_SCORE as const;
    public scores: Map<string, number>;

    constructor(_last: LocalGameStateResolving, scores: Map<string, number>) {
        super();
        this.scores = scores;
    }

    public timeOver(instructions: DrawingInstructions): LocalGameStateDrawing {
        return new LocalGameStateDrawing(this, instructions);
    }

    public toRemote(): RemoteGameStateRoundEndScore {
        const { state } = this;
        return { state };
    }
}

export type LocalGameState =
    | LocalGameStateLobby
    | LocalGameStateStarted
    | LocalGameStateDrawing
    | LocalGameStateDrawingDone
    | LocalGameStateWaitingForImages
    | LocalGameStateWaitingForOtherPlayersImages
    | LocalGameStateSuggesting
    | LocalGameStateSuggestingDone
    | LocalGameStateGuessing
    | LocalGameStateGuessingDone
    | LocalGameStateResolving
    | LocalGameStateRoundEndScore;
