import { WordGenerator, WordQuery } from "./word-generator";
import { addSeconds } from "date-fns";
import { EventEmitter } from "typed-event-emitter";
import { RemoteUsers } from "../remote-users";
import { GameConfig, Difficulty, DrawingInstructions, ImageInfo, SuggestingInstructions, GuessingInstructions } from "../types";
import bind from "bind-decorator";
import { shuffle } from "../utils";

function difficultyToQuartiles(difficulty: Difficulty): [number, number] {
    switch (difficulty) {
        case Difficulty.EASY:
            return [0, 0.25];
        case Difficulty.MEDIUM:
            return [0.25, 0.75];
        case Difficulty.HARD:
            return [0.75, 1];
    }
}

export class GameController extends EventEmitter {
    public scores = new Map<string, number>();
    public wordGenerator = new WordGenerator();

    public images = new Map<string, ImageInfo>();
    public suggestions = new Map<string, string>();
    public guesses = new Map<string, string>();
    public doneImages = new Set<string>();
    public currentImageId: string | undefined;

    public onGameStart = this.registerEvent<(config: GameConfig) => void>();
    public onDrawingStart = this.registerEvent<(instructions: Map<string, DrawingInstructions>) => void>();
    public onSuggestingStart = this.registerEvent<(instructions: SuggestingInstructions) => void>();
    public onGuessingStart = this.registerEvent<(instructions: GuessingInstructions) => void>();
    public onAllImagesReceived = this.registerEvent<(images: Map<string, ImageInfo>) => void>();
    public onResolvingStart = this.registerEvent<(scores: Map<string, number>) => void>();
    public onRoundEnd = this.registerEvent<(scores: Map<string, number>) => void>();

    // private drawingTimeout: number | undefined;
    // private suggestingTimeout: number | undefined;
    // private guessingTimeout: number | undefined;

    constructor(private users: RemoteUsers, public config: GameConfig) {
        super();
        users.allPlayerUsers.forEach(({ id }) => this.scores.set(id, 0));
    }

    public async load() {
        await this.wordGenerator.load(this.config.dictionary);
    }

    public start() {
        this.emit(this.onGameStart, this.config);
        this.startDrawing();
    }

    private get wordQuery(): WordQuery {
        const [minQuartile, maxQuartile] = difficultyToQuartiles(this.config.difficulty);
        return {
            wordTypes: this.config.wordTypes,
            minQuartile,
            maxQuartile,
        };
    }

    private startDrawing() {
        this.images = new Map();
        this.doneImages = new Set();
        const instructions = new Map<string, DrawingInstructions>();
        const deadline = addSeconds(new Date(), this.config?.drawingDuration);
        this.users.allPlayerUsers.forEach(({ id }) => {
            instructions.set(id, {
                deadline,
                words: this.wordGenerator.consumeMany(this.wordQuery, this.config.wordOptionCount),
            });
        });
        this.emit(this.onDrawingStart, instructions);
        // this.drawingTimeout = window.setTimeout(this.startSuggesting, this.config.drawingDuration * 1000);
    }

    public submitImage(userId: string, image: ImageInfo) {
        this.images.set(userId, image);
        if (this.images.size === this.users.playerCount) {
            // clearTimeout(this.drawingTimeout);
            // this.drawingTimeout = undefined;
            // this.startSuggesting();
            this.emit(this.onAllImagesReceived, this.images);
        }
    }

    @bind public startSuggesting() {
        this.suggestions = new Map();
        this.guesses = new Map();
        this.currentImageId = shuffle(Array.from(this.images.values()).map(({ id }) => id)).find(
            id => !this.doneImages.has(id),
        );
        if (!this.currentImageId) {
            throw new Error("Out of images.");
        }
        this.doneImages.add(this.currentImageId);
        const deadline = addSeconds(new Date(), this.config.suggestingDuration);
        this.emit(this.onSuggestingStart, { deadline, imageId: this.currentImageId });
        // this.suggestingTimeout = window.setTimeout(this.startGuessing, this.config.suggestingDuration * 1000);
    }

    public submitSuggestion(userId: string, suggestion: string) {
        this.suggestions.set(userId, suggestion);
        if (this.suggestions.size === this.users.playerCount) {
            // clearTimeout(this.suggestingTimeout);
            // this.suggestingTimeout = undefined;
            this.startGuessing();
        }
    }

    @bind private startGuessing() {
        if (!this.currentImageId) {
            throw new Error("Guessing started before image was picked.");
        }
        this.guesses = new Map();
        const deadline = addSeconds(new Date(), this.config.suggestingDuration);
        const instructions = {
            options: shuffle([...Array.from(this.suggestions.values()), this.currentImage!.word.word]),
            deadline,
            imageId: this.currentImageId,
        };
        this.emit(this.onGuessingStart, instructions);
        // this.guessingTimeout = window.setTimeout(this.startResolving, this.config.guessingDuration * 1000);
    }

    public submitGuess(userId: string, option: string) {
        this.guesses.set(userId, option);
        if (this.guesses.size === this.users.playerCount) {
            // clearTimeout(this.guessingTimeout);
            // this.guessingTimeout = undefined;
            this.startResolving();
        }
    }

    private get currentImage(): ImageInfo | undefined {
        return this.currentImageId ? this.images.get(this.currentImageId) : undefined;
    }

    public addScore(userId: string, score: number): void {
        this.scores.set(userId, (this.scores.get(userId) ?? 0) + score);
    }

    @bind private startResolving() {
        for (const user of this.users.allPlayerUsers) {
            if (this.currentImage!.word.word.toLocaleLowerCase() === this.guesses.get(user.id)) {
                this.addScore(user.id, this.config.pointsForCorrectGuess);
                this.addScore(this.currentImage!.userId, this.config.pointsForCorrectGuess);
            }
            const suggestion = this.suggestions.get(user.id);
            if (suggestion) {
                this.addScore(
                    user.id,
                    Array.from(this.guesses.values()).reduce(
                        (sum, guess) =>
                            guess.toLocaleLowerCase() === suggestion.toLocaleLowerCase()
                                ? sum + this.config.pointsForBamboozle
                                : sum,
                        0,
                    ),
                );
            }
        }
        this.guesses = new Map();
        this.suggestions = new Map();
        this.currentImageId = undefined;
        this.emit(this.onResolvingStart, this.scores);
        if (this.doneImages.size === this.images.size) {
            setTimeout(this.startRoundDone, this.config.scoreAfterImageDuration);
        } else {
            setTimeout(this.startSuggesting, this.config.scoreAfterImageDuration);
        }
    }

    @bind private startRoundDone() {
        this.images = new Map();
        this.doneImages = new Set();
        this.emit(this.onRoundEnd, this.scores);
        setTimeout(this.startDrawing, this.config.scoreAfterRoundDuration);
    }
}
