import { computed, action, observable, IObservable, IObservableValue } from "mobx";
import { Peer, Host, createHost, createClient, Client } from "../networking";
import { NetworkingMode, RemoteUser, GameStateType, WordInfo } from "../types";
import { RemoteUsers } from "../remote-users";
import { LocalGameStateLobby, LocalGameState } from "./local-game-state";
import { component } from "tsdi";
import { GameController } from "./game-controller";
import { GameConfig } from "../types";
import { v4 } from "uuid";

@component
export class Game {
    public users = new RemoteUsers();
    @observable.shallow public peer: Peer | undefined;
    public gameStateBox: IObservableValue<LocalGameState> = observable.box(new LocalGameStateLobby());

    private gameController: GameController | undefined;

    @computed public get gameState(): LocalGameState {
        return this.gameStateBox.get();
    }

    @action.bound private updateGameState(gameState: LocalGameState) {
        this.gameStateBox.set(gameState);
    }

    @computed public get networkMode() {
        if (!this.peer) {
            return NetworkingMode.DISCONNECTED
        }
        if (this.peer instanceof Host) {
            return NetworkingMode.HOST;
        }
        return NetworkingMode.CLIENT;
    }

    @action.bound public drawingDone(pickedWord: WordInfo, data: string) {
        if (this.gameState.state !== GameStateType.DRAWING) {
            throw new Error(`Game state is in invalid state ${this.gameState.state} for drawingDone().`);
        }
        this.updateGameState(this.gameState.drawingDone(pickedWord, data));
        this.peer?.sendGameState(this.gameState.toRemote());
    }

    @action.bound public suggestingDone(suggestion: string) {
        if (this.gameState.state !== GameStateType.SUGGESTING) {
            throw new Error(`Game state is in invalid state ${this.gameState.state} for suggestingDone().`);
        }
        this.updateGameState(this.gameState.suggestingDone(suggestion));
        this.peer?.sendGameState(this.gameState.toRemote());
    }

    @action.bound public guessingDone(pickedOption: string) {
        if (this.gameState.state !== GameStateType.GUESSING) {
            throw new Error(`Game state is in invalid state ${this.gameState.state} for guessingDone().`);
        }
        this.updateGameState(this.gameState.guessingDone(pickedOption));
        this.peer?.sendGameState(this.gameState.toRemote());
    }

    @action.bound public startGame() {
        if (this.networkMode === NetworkingMode.HOST) {
            this.gameController?.start();
        }
    }

    public async initialize(config: GameConfig): Promise<void>;
    public async initialize(networkId: string): Promise<void>;
    @action.bound public async initialize(arg1?: string | GameConfig): Promise<void> {
        this.peer = typeof arg1 === "string" ? new Client() : new Host(this.users);

        this.peer.onWelcome(users => {
            this.users.add(...users);
        });
        this.peer.onUserConnected(user => {
            this.users.add(user);
        });
        this.peer.onUserDisconnected(userId => {
            this.users.remove(userId);
        }) ;
        this.peer.onGameStart(() => {
            if (this.gameState.state !== GameStateType.LOBBY) {
                throw new Error(`Event "game start" is invalid in game state ${this.gameState.state}.`);
            }
            this.updateGameState(this.gameState.startGame());
        });
        this.peer.onDrawingStart(instructions => {
            if (this.gameState.state !== GameStateType.STARTED) {
                throw new Error(`Event "drawing start" is invalid in game state ${this.gameState.state}.`);
            }
            this.updateGameState(this.gameState.startDrawing(instructions));
        });
        this.peer.onDrawingTimeOver(() => {
            if (this.gameState.state !== GameStateType.DRAWING && this.gameState.state !== GameStateType.DRAWING_DONE) {
                throw new Error(`Event "drawing time over" is invalid in game state ${this.gameState.state}.`);
            }
            this.updateGameState(this.gameState.timeOver());
        });
        this.peer.onImages(images => {
            if (this.gameState.state !== GameStateType.WAITING_FOR_IMAGES) {
                throw new Error(`Event "images" is invalid in game state ${this.gameState.state}.`);
            }
            this.updateGameState(this.gameState.images(images));
        });
        this.peer.onSuggestingStart(instructions => {
            if (this.gameState.state !== GameStateType.WAITING_FOR_OTHER_PLAYERS_IMAGES) {
                throw new Error(`Event "suggesting start" is invalid in game state ${this.gameState.state}.`);
            }
            this.updateGameState(this.gameState.allPlayersDoneLoading(instructions));
        });
        this.peer.onGuessingStart(instructions => {
            if (this.gameState.state !== GameStateType.SUGGESTING && this.gameState.state !== GameStateType.SUGGESTING_DONE) {
                throw new Error(`Event "guessing start" is invalid in game state ${this.gameState.state}.`);
            }
            this.updateGameState(this.gameState.timeOver(instructions));
        });
        this.peer.onResolvingStart(() => {
            if (this.gameState.state !== GameStateType.GUESSING && this.gameState.state !== GameStateType.GUESSING_DONE) {
                throw new Error(`Event "resolving start" is invalid in game state ${this.gameState.state}.`);
            }
            this.updateGameState(this.gameState.timeOver());
        });
        this.peer.onRoundEndScore((scores) => {
            if (this.gameState.state !== GameStateType.RESOLVING) {
                throw new Error(`Event "round end score" is invalid in game state ${this.gameState.state}.`);
            }
            this.updateGameState(this.gameState.timeOver(scores));
        });

        if (this.peer instanceof Host) {
            await this.peer.host();
        }
        if (this.peer instanceof Client) {
            await this.peer.connect(arg1 as string, this.users.ownUser);
        }

        if (this.networkMode === NetworkingMode.HOST) {
            this.gameController = new GameController(this.users, arg1 as GameConfig);
            const hostPeer = this.peer as Host;
            const usersImageLoadingDone = new Set();
            hostPeer.onClientGameStateChanged((userId, gameState) => {
                if (!this.gameController) {
                    return;
                }
                switch (gameState.state) {
                    case GameStateType.WAITING_FOR_OTHER_PLAYERS_IMAGES:
                        usersImageLoadingDone.add(userId);
                        if (usersImageLoadingDone.size === this.users.allPlayerUsers.length) {
                            usersImageLoadingDone.clear();
                            this.gameController?.startSuggesting();
                        }
                        break;
                    case GameStateType.DRAWING_DONE:
                        this.gameController.submitImage(userId, {
                            id: v4(),
                            userId,
                            word: gameState.pickedWord,
                            data: gameState.data!,
                        });
                        break;
                    case GameStateType.SUGGESTING_DONE:
                        this.gameController.submitSuggestion(userId, gameState.suggestion);
                        break;
                    case GameStateType.GUESSING_DONE:
                        this.gameController.submitGuess(userId, gameState.pickedOption);
                        break;
                }
            });

            this.gameController.onGameStart(() => {
                hostPeer.sendGameStart();
            })
            this.gameController.onDrawingStart(instructionsMap => {
                for (const [userId, userInstructions] of instructionsMap) {
                    hostPeer.sendDrawingStart(userId, userInstructions);
                }
                for (const { id } of this.users.allSpectatorUsers) {
                    hostPeer.sendDrawingStart(id);
                }
            });
            this.gameController.onSuggestingStart(instructions => {
                hostPeer.sendSuggestingStart(instructions);
            });
            this.gameController.onGuessingStart(instructions => {
                hostPeer.sendGuessingStart(instructions);
            });
            this.gameController.onResolvingStart(scores => {
                hostPeer.sendResolvingStart(scores);
            });
            this.gameController.onRoundEnd(scores => {
                hostPeer.sendRoundEndScore(scores);
            });
            await this.gameController.load();
        }
    }
}