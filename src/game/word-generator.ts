import { shuffle } from "../utils";
import { WordInfo, WordType } from "../types";

export interface WordQuery {
    wordTypes?: Set<WordType>;
    maxFrequency?: number;
    minFrequency?: number;
    maxQuartile?: number;
    minQuartile?: number;
}

export class WordGenerator {
    private usedWords = new Set<string>();
    private words: WordInfo[] = [];
    private sortedFrequencies: number[] = [];

    public async load(dictionary: string) {
        const request = await fetch(`dicts/${dictionary}.json`);
        const json = (await request.json()) as [string, string, number][];
        const shuffled = shuffle(json);
        this.words = shuffled.map(([word, wordType, frequency], index) => ({
            id: `${index}`,
            word,
            wordType: wordType as WordType,
            frequency,
        }));
        this.sortedFrequencies = [...this.words].map(word => word.frequency).sort((a, b) => a - b);
    }

    private findAll(query: WordQuery): WordInfo[] {
        return this.words.filter(wordInfo => this.matchesQuery(wordInfo, query));
    }

    private quartile(quartile: number): number {
        const index = (this.sortedFrequencies.length - 1) * quartile;
        const base = Math.floor(index);
        const rest = index - base;
        if (this.sortedFrequencies[base + 1] !== undefined) {
            return this.sortedFrequencies[base] + rest * (this.sortedFrequencies[base + 1] - this.sortedFrequencies[base]);
        }
        return this.sortedFrequencies[base];
    }

    private matchesQuery(
        { wordType, frequency }: WordInfo,
        { wordTypes, maxFrequency, minFrequency, minQuartile, maxQuartile }: WordQuery,
    ): boolean {
        if (wordTypes && !wordTypes.has(wordType)) {
            return false;
        }
        if (maxFrequency && frequency > maxFrequency) {
            return false;
        }
        if (minFrequency && frequency < minFrequency) {
            return false;
        }
        if (maxQuartile && frequency > this.quartile(maxQuartile)) {
            return false;
        }
        if (minQuartile && frequency < this.quartile(minQuartile)) {
            return false;
        }
        return true;
    }

    public count(query: WordQuery): number {
        return this.findAll(query).length;
    }

    public consumeOne(query: WordQuery): WordInfo {
        for (const word of this.words) {
            if (this.usedWords.has(word.id) || !this.matchesQuery(word, query)) {
                continue;
            }
            this.usedWords.add(word.id);
            return word;
        }
        throw new Error("Out of words.");
    }

    public consumeMany(query: WordQuery, count: number): WordInfo[] {
        const result: WordInfo[] = [];
        while (result.length < count) {
            result.push(this.consumeOne(query));
        }
        return result;
    }
}
