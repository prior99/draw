
import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Game } from "../../game";
import "./state-drawing.scss";
import { SketchField } from "@hsluoyz/react-sketch/dist";

@external
@observer
export class StateDrawing extends React.Component {
    @inject private game!: Game;

    public render() {
        return (
            <div>
                <SketchField />
            </div>
        );
    }
}