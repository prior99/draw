
import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Game } from "../../game";
import "./state-guessing.scss";

@external
@observer
export class StateGuessing extends React.Component {
    @inject private game!: Game;

    public render() {
        return (
            <div></div>
        );
    }
}