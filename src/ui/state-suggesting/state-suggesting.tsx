
import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Game } from "../../game";
import "./state-suggesting.scss";

@external
@observer
export class StateSuggesting extends React.Component {
    @inject private game!: Game;

    public render() {
        return (
            <div></div>
        );
    }
}