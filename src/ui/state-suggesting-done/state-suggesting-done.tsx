
import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Game } from "../../game";
import "./state-suggesting-done.scss";

@external
@observer
export class StateSuggestingDone extends React.Component {
    @inject private game!: Game;

    public render() {
        return (
            <div></div>
        );
    }
}