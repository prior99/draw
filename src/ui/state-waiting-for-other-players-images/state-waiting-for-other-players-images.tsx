
import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Game } from "../../game";
import "./state-waiting-for-other-players-images.scss";

@external
@observer
export class StateWaitingForOtherPlayersImages extends React.Component {
    @inject private game!: Game;

    public render() {
        return (
            <div></div>
        );
    }
}