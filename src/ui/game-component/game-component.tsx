import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Game } from "../../game";
import { GameStateType } from "../../types";
import { computed } from "mobx";
import { StateLobby } from "../state-lobby";
import { StateStarted } from "../state-started";
import { StateDrawing } from "../state-drawing";
import { StateDrawingDone } from "../state-drawing-done";
import { StateWaitingForOtherPlayersImages } from "../state-waiting-for-other-players-images";
import { StateSuggesting } from "../state-suggesting";
import { StateSuggestingDone } from "../state-suggesting-done";
import { StateGuessingDone } from "../state-guessing-done";
import { StateGuessing } from "../state-guessing";
import { StateResolving } from "../state-resolving";
import { StateRoundEndScore } from "../state-round-end-score";
import { StateWaitingForImages } from "../state-waiting-for-images";

@external
@observer
export class GameComponent extends React.Component<{}> {
    @inject private game!: Game;

    @computed private get gameState(): GameStateType {
        return this.game.gameState.state;
    }

    public render() {
        switch (this.gameState) {
            default:
            case GameStateType.LOBBY:
                return <StateLobby />;
            case GameStateType.STARTED:
                return <StateStarted />;
            case GameStateType.DRAWING:
                return <StateDrawing />;
            case GameStateType.DRAWING_DONE:
                return <StateDrawingDone />;
            case GameStateType.WAITING_FOR_IMAGES:
                return <StateWaitingForImages />;
            case GameStateType.WAITING_FOR_OTHER_PLAYERS_IMAGES:
                return <StateWaitingForOtherPlayersImages />;
            case GameStateType.SUGGESTING:
                return <StateSuggesting />;
            case GameStateType.SUGGESTING_DONE:
                return <StateSuggestingDone />;
            case GameStateType.GUESSING:
                return <StateGuessing />;
            case GameStateType.GUESSING_DONE:
                return <StateGuessingDone />;
            case GameStateType.RESOLVING:
                return <StateResolving />;
            case GameStateType.ROUND_END_SCORE:
                return <StateRoundEndScore />;
        }
    }
}
