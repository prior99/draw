
import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Game } from "../../game";
import "./state-waiting-for-images.scss";

@external
@observer
export class StateWaitingForImages extends React.Component {
    @inject private game!: Game;

    public render() {
        return (
            <div></div>
        );
    }
}