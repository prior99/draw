
import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { LobbyMode, UserMode, NetworkingMode } from "../../types";
import { Game } from "../../game";
import { action, computed } from "mobx";
import { Button, Segment, List, Grid, Popup, Message } from "semantic-ui-react";
import "./state-lobby.scss";
import { routeGame } from "../../pages";
import { MenuContainer } from "../menu-container";

@external
@observer
export class StateLobby extends React.Component {
    @inject private game!: Game;
    @computed private get hasClipboardApi() {
        const anyNavigator = navigator as any;
        return Boolean(anyNavigator.clipboard);
    }

    private connectUrl(userMode: UserMode) {
        return `${location.origin}/#${routeGame.path(LobbyMode.CLIENT, userMode, this.game.peer?.networkId)}`;
    }

    @computed private get popupText() {
        if (this.hasClipboardApi) {
            return "Copied to clipboard.";
        }
        return `Can't copy to clipboard: "${this.connectUrl}".`;
    }

    @action.bound private handleStartGame() {
        this.game.startGame();
    }
    private handleIdClick(userMode: UserMode) {
        return async () => {
            if (this.hasClipboardApi) {
                const anyNavigator = navigator as any;
                await anyNavigator.clipboard.writeText(this.connectUrl(userMode));
            }
        };
    }

    public render() {
        return (
            <MenuContainer>
                <Grid className="StateLobby__grid">
                    <Grid.Row className="StateLobby__row">
                        <Grid.Column width={8}>
                            <Popup
                                on="click"
                                trigger={
                                    <Message
                                        icon="globe"
                                        color="blue"
                                        onClick={this.handleIdClick(UserMode.SPECTATOR)}
                                        content="Invite spectator"
                                        className="StateLobby__message"
                                    />
                                }
                                content={this.popupText}
                            />
                        </Grid.Column>
                        <Grid.Column width={8}>
                            <Popup
                                on="click"
                                trigger={
                                    <Message
                                        icon="globe"
                                        color="blue"
                                        onClick={this.handleIdClick(UserMode.PLAYER)}
                                        content="Invite player"
                                        className="StateLobby__message"
                                    />
                                }
                                content={this.popupText}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Segment>
                                <h1>Users</h1>
                                <List relaxed>
                                    {this.game.users.all.map(user => (
                                        <List.Item icon="user" key={user.user.id}>
                                            {user.user.userMode === UserMode.PLAYER ? user.user.name : "Spectator"}
                                        </List.Item>
                                    ))}
                                </List>
                                {this.game.networkMode === NetworkingMode.HOST && (
                                    <Button onClick={this.handleStartGame}>Start</Button>
                                )}
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </MenuContainer>
        );
    }
}