import PeerJS from "peerjs";
import { bind } from "bind-decorator";
import { v4 } from "uuid";
import { EventEmitter } from "typed-event-emitter";
import {
    RemoteUser,
    HostMessage,
    HostMessageType,
    DrawingInstructions,
    ImageInfo,
    GuessingInstructions,
    ClientMessageType,
    ClientMessage,
    RemoteGameState,
    SuggestingInstructions,
} from "../types";
import { observable } from "mobx";

export abstract class Peer extends EventEmitter {
    protected peer?: PeerJS;
    @observable public peerId = v4();
    @observable public networkId: string | undefined = undefined;

    public onWelcome = this.registerEvent<(users: RemoteUser[]) => void>();
    public onUserConnected = this.registerEvent<(user: RemoteUser) => void>();
    public onUserDisconnected = this.registerEvent<(userId: string) => void>();
    public onGameStart = this.registerEvent<() => void>();
    public onDrawingStart = this.registerEvent<(instructions: DrawingInstructions) => void>();
    public onDrawingTimeOver = this.registerEvent<() => void>();
    public onImages = this.registerEvent<(images: Map<string, ImageInfo>) => void>();
    public onSuggestingStart = this.registerEvent<(instructions: SuggestingInstructions) => void>();
    public onGuessingStart = this.registerEvent<(instructions: GuessingInstructions) => void>();
    public onResolvingStart = this.registerEvent<() => void>();
    public onRoundEndScore = this.registerEvent<(scores: Map<string, number>) => void>();

    protected abstract sendClientMessage(message: ClientMessage): void;

    @bind protected handleHostMessage(message: HostMessage) {
        console.info(`Received host message:`, message);
        switch (message.message) {
            case HostMessageType.WELCOME:
                return this.emit(this.onWelcome, message.users);
            case HostMessageType.USER_CONNECTED:
                return this.emit(this.onUserConnected, message.user);
            case HostMessageType.USER_DISCONNECTED:
                return this.emit(this.onUserDisconnected, message.userId);
            case HostMessageType.GAME_START:
                return this.emit(this.onGameStart);
            case HostMessageType.DRAWING_START:
                return this.emit(this.onDrawingStart, message.instructions);
            case HostMessageType.DRAWING_TIME_OVER:
                return this.emit(this.onDrawingTimeOver);
            case HostMessageType.IMAGES:
                return this.emit(this.onImages, new Map(message.images));
            case HostMessageType.SUGGESTING_START:
                return this.emit(this.onSuggestingStart, message.instructions);
            case HostMessageType.GUESSING_START:
                return this.emit(this.onGuessingStart, message.instructions);
            case HostMessageType.RESOLVING_START:
                return this.emit(this.onResolvingStart);
            case HostMessageType.ROUND_END_SCORE:
                return this.emit(this.onRoundEndScore, new Map(message.scores));
        }
    }

    @bind public async close(): Promise<void> {
        if (!this.peer) {
            return;
        }
        this.peer.destroy();
    }

    @bind protected async open(): Promise<string> {
        await new Promise(resolve => {
            this.peer = new PeerJS(null as any); // eslint-disable-line
            this.peer.on("open", () => resolve());
        });
        if (!this.peer) {
            throw new Error("Connection id could not be determined.");
        }
        console.info(`Connection open. Peer id is ${this.peer.id}.`)
        return this.peer.id;
    }

    @bind protected sendToPeer(connection: PeerJS.DataConnection, message: HostMessage | ClientMessage): void {
        connection.send(message);
    }

    @bind public sendHello(user: RemoteUser): void {
        this.sendClientMessage({
            message: ClientMessageType.HELLO,
            user,
            originPeerId: this.peerId,
        });
    }

    @bind public sendGameState(gameState: RemoteGameState): void {
        this.sendClientMessage({
            message: ClientMessageType.GAME_STATE_CHANGE,
            gameState,
            originPeerId: this.peerId,
        });
    }
}
