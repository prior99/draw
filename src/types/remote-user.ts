export enum UserMode {
    PLAYER = "player",
    SPECTATOR = "spectator",
}

export interface RemotePlayer {
    userMode: UserMode.PLAYER;
    name: string;
    id: string;
}

export interface RemoteSpectator {
    userMode: UserMode.SPECTATOR;
    id: string;
}

export type RemoteUser = RemotePlayer | RemoteSpectator;