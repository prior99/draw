import { RemoteUser } from "./remote-user";
import { RemoteGameState } from "./remote-game-state";
import { WordInfo } from "./words";

export enum HostMessageType {
    WELCOME = "welcome",
    USER_CONNECTED = "user connected",
    USER_DISCONNECTED = "user disconnected",
    GAME_START = "game start",
    DRAWING_START = "drawing start",
    DRAWING_TIME_OVER = "drawing time_over",
    IMAGES = "images",
    SUGGESTING_START = "suggesting start",
    GUESSING_START = "guessing start",
    RESOLVING_START = "resolving start",
    ROUND_END_SCORE = "round end score",
    RELAYED_GAME_STATE_CHANGE = "relayed game state change"
}

export interface HostMessageWelcome {
    message: HostMessageType.WELCOME;
    users: RemoteUser[];
}

export interface HostMessageUserConnected {
    message: HostMessageType.USER_CONNECTED;
    user: RemoteUser;
}

export interface HostMessageUserDisconnected {
    message: HostMessageType.USER_DISCONNECTED;
    userId: string;
}

export interface HostMessageGameStart {
    message: HostMessageType.GAME_START;
}

export interface DrawingInstructions {
    words: WordInfo[];
    deadline: Date;
}

export interface HostMessageDrawingStart {
    message: HostMessageType.DRAWING_START;
    instructions?: DrawingInstructions;
}

export interface HostMessageDrawingTimeOver {
    message: HostMessageType.DRAWING_TIME_OVER;
}

export interface ImageInfo {
    id: string;
    userId: string;
    word: WordInfo;
    data: string;
}

export interface HostMessageImages {
    message: HostMessageType.IMAGES;
    images: [string, ImageInfo][];
}

export interface SuggestingInstructions {
    deadline: Date;
    imageId: string;
}

export interface HostMessageSuggestingStart {
    message: HostMessageType.SUGGESTING_START;
    instructions: SuggestingInstructions;
}

export interface GuessingInstructions {
    options: string[];
    deadline: Date;
    imageId: string;
}

export interface HostMessageGuessingStart {
    message: HostMessageType.GUESSING_START;
    instructions: GuessingInstructions;
}

export interface HostMessageResolvingStart {
    message: HostMessageType.RESOLVING_START;
    scores: [string, number][];
}

export interface HostMessageRoundEndScore {
    message: HostMessageType.ROUND_END_SCORE;
    scores: [string, number][];
}

export interface HostRelayedGameStateChange {
    message: HostMessageType.RELAYED_GAME_STATE_CHANGE;
    gameState: RemoteGameState;
    userId: string;
}

export type HostMessage =
    | HostMessageWelcome
    | HostMessageUserConnected
    | HostMessageUserDisconnected
    | HostMessageGameStart
    | HostMessageDrawingStart
    | HostMessageDrawingTimeOver
    | HostMessageImages
    | HostMessageSuggestingStart
    | HostMessageGuessingStart
    | HostMessageResolvingStart
    | HostMessageRoundEndScore
    | HostRelayedGameStateChange;
