export const enum WordType {
    ADJECTIVE = "adjective",
    NOUN = "noun",
    VERB = "verb",
}

export interface WordInfo {
    id: string;
    word: string;
    frequency: number;
    wordType: WordType;
}