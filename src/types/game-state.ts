export const enum GameStateType {
    LOBBY = "lobby",
    STARTED = "started",
    DRAWING = "drawing",
    DRAWING_DONE = "drawing done",
    WAITING_FOR_IMAGES = "waiting for images",
    WAITING_FOR_OTHER_PLAYERS_IMAGES = "waiting for other players images",
    SUGGESTING = "presenting image",
    SUGGESTING_DONE = "suggesting done",
    GUESSING = "guessing",
    GUESSING_DONE = "guessing done",
    RESOLVING = "resolving",
    ROUND_END_SCORE = "round end score",
}