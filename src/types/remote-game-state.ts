import { GameStateType } from "./game-state";
import { DrawingInstructions } from "./host-messages";
import { WordInfo } from "./words";

export interface RemoteBaseGameState {
    state: GameStateType;
}

export interface RemoteGameStateLobby extends RemoteBaseGameState {
    state: GameStateType.LOBBY;
}

export interface RemoteGameStateStarted extends RemoteBaseGameState {
    state: GameStateType.STARTED;
}

export interface RemoteGameStateDrawing extends RemoteBaseGameState {
    state: GameStateType.DRAWING;
    instructions: DrawingInstructions;
}

export interface RemoteGameStateDrawingDone extends RemoteBaseGameState {
    state: GameStateType.DRAWING_DONE;
    instructions: DrawingInstructions;
    pickedWord: WordInfo;
    data?: string;
}

export interface RemoteGameStateWaitingForImages extends RemoteBaseGameState {
    state: GameStateType.WAITING_FOR_IMAGES;
}

export interface RemoteGameStateWaitingForOtherPlayersImages extends RemoteBaseGameState {
    state: GameStateType.WAITING_FOR_OTHER_PLAYERS_IMAGES;
}

export interface RemoteGameStateSuggesting extends RemoteBaseGameState {
    state: GameStateType.SUGGESTING;
}

export interface RemoteGameStateSuggestingDone extends RemoteBaseGameState {
    state: GameStateType.SUGGESTING_DONE;
    suggestion: string;
}

export interface RemoteGameStateGuessing extends RemoteBaseGameState {
    state: GameStateType.GUESSING;
}

export interface RemoteGameStateGuessingDone extends RemoteBaseGameState {
    state: GameStateType.GUESSING_DONE;
    pickedOption: string;
}

export interface RemoteGameStateResolving extends RemoteBaseGameState {
    state: GameStateType.RESOLVING;
    pickedOption: string | undefined;
}

export interface RemoteGameStateRoundEndScore extends RemoteBaseGameState {
    state: GameStateType.ROUND_END_SCORE;
}

export type RemoteGameState =
    | RemoteGameStateLobby
    | RemoteGameStateStarted
    | RemoteGameStateDrawing
    | RemoteGameStateDrawingDone
    | RemoteGameStateWaitingForImages
    | RemoteGameStateWaitingForOtherPlayersImages
    | RemoteGameStateSuggesting
    | RemoteGameStateSuggestingDone
    | RemoteGameStateGuessing
    | RemoteGameStateGuessingDone
    | RemoteGameStateResolving
    | RemoteGameStateRoundEndScore;