import { WordType } from "./words";

export const enum Difficulty {
    EASY = "easy",
    MEDIUM = "medium",
    HARD = "hard",
}

export const enum Dictionary {
    GERMAN = "german",
}

export interface GameConfig {
    wordTypes: Set<WordType>;
    difficulty: Difficulty;
    dictionary: Dictionary;
    drawingDuration: number;
    suggestingDuration: number;
    guessingDuration: number;
    wordOptionCount: number;
    scoreAfterImageDuration: number;
    scoreAfterRoundDuration: number;
    pointsForCorrectGuess: number;
    pointsForBamboozle: number;
}
