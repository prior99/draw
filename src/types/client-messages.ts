import { RemoteUser } from "./remote-user";
import { RemoteGameState } from "./remote-game-state";

export enum ClientMessageType {
    HELLO = "hello",
    GAME_STATE_CHANGE = "game state change",
}

export interface BaseClientMessage {
    originPeerId: string;
}

export interface ClientMessageHello extends BaseClientMessage {
    message: ClientMessageType.HELLO;
    user: RemoteUser;
}

export interface ClientMessageGameStateChange extends BaseClientMessage  {
    message: ClientMessageType.GAME_STATE_CHANGE;
    gameState: RemoteGameState;
}

export type ClientMessage =
    | ClientMessageHello
    | ClientMessageGameStateChange;