import * as React from "react";
import { addRoute, RouteProps } from "../../routing";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { LobbyMode, UserMode, WordType, Difficulty, Dictionary } from "../../types";
import { Game } from "../../game";
import { GameComponent } from "../../ui";

export interface PageGameProps {
    lobbyMode: LobbyMode;
    userMode: UserMode;
    id?: string;
}

@external
@observer
export class PageGame extends React.Component<RouteProps<PageGameProps>> {
    @inject private game!: Game;

    async componentDidMount() {
        if (this.props.match.params.lobbyMode === LobbyMode.HOST) {
            await this.game.initialize({
                wordTypes: new Set([WordType.ADJECTIVE, WordType.NOUN, WordType.VERB]),
                difficulty: Difficulty.MEDIUM,
                dictionary: Dictionary.GERMAN,
                drawingDuration: 90,
                suggestingDuration: 20,
                guessingDuration: 10,
                wordOptionCount: 3,
                scoreAfterImageDuration: 7,
                scoreAfterRoundDuration: 10,
                pointsForCorrectGuess: 1000,
                pointsForBamboozle: 500,
            });
        } else {
            await this.game.initialize(this.props.match.params.id!);
        }
    }

    public render() {
        return (
            <GameComponent />
        );
    }
}

export const routeGame = addRoute<PageGameProps>({
    path: (lobbyMode: LobbyMode, userMode: UserMode, id?: string) => {
        switch (lobbyMode) {
            case LobbyMode.CLIENT:
                return `/game/client/${userMode}/${id}`;
            case LobbyMode.HOST:
                return `/game/host/${userMode}`;
        }
    },
    pattern: "/game/:lobbyMode/:userMode/:id?",
    component: PageGame,
});
