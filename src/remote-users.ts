import { observable, computed, action } from "mobx";
import { RemoteUser, RemoteGameState, UserMode, GameStateType } from "./types";
import { v4 } from "uuid";
import { generateUserName } from "./utils";

interface UserAndGameState {
    user: RemoteUser;
    gameState: RemoteGameState;
}

export class RemoteUsers {
    @observable public users = new Map<string, UserAndGameState>();

    private ownId: string;

    constructor() {
        const user: RemoteUser = {
            userMode: UserMode.PLAYER,
            id: v4(),
            name: generateUserName(),
        };
        this.users.set(user.id, { user, gameState: { state: GameStateType.LOBBY } }), (this.ownId = user.id);
    }

    @computed public get ownUser(): RemoteUser {
        return this.getUser(this.ownId)!;
    }

    @computed public get ownGameState(): RemoteGameState {
        return this.getGameState(this.ownId)!;
    }

    @action.bound public remove(userId: string): void {
        this.users.delete(userId);
    }

    @action.bound public add(...users: RemoteUser[]): void {
        users.forEach(user =>
            this.users.set(user.id, {
                user,
                gameState: { state: GameStateType.LOBBY },
            }),
        );
    }

    @computed public get all(): UserAndGameState[] {
        return Array.from(this.users.values());
    }

    @computed public get allPlayers(): UserAndGameState[] {
        return this.all.filter(({ user }) => user.userMode === UserMode.PLAYER);
    }

    @computed public get allSpectatorUsers(): RemoteUser[] {
        return this.all.filter(({ user }) => user.userMode === UserMode.SPECTATOR).map(({ user }) => user);
    }

    @computed public get allPlayerUsers(): RemoteUser[] {
        return this.all.filter(({ user }) => user.userMode === UserMode.PLAYER).map(({ user }) => user);
    }

    @computed public get playerCount(): number {
        return this.allPlayers.length;
    }

    public getUser(id: string): RemoteUser | undefined {
        return this.users.get(id)?.user;
    }

    public getGameState(id: string): RemoteGameState | undefined {
        return this.users.get(id)?.gameState;
    }

    @action.bound public setOwnUser(user: RemoteUser) {
        this.users.set(this.ownId, {
            user: { id: this.ownId, ...user },
            gameState: this.ownGameState,
        });
    }
}
