export function randomRelationships<T>(input: T[], random = Math.random): Map<T, T> {
    const stride = random() * (input.length - 1) + 1;
    const result = new Map<T, T>();
    for (let index = 0; index < input.length; ++index) {
        result.set(input[index], input[index + stride]);
    }
    return result;
}