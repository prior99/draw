SELECT
    json_agg(info)
FROM (
    SELECT
        json_build_array(word, type, round(frequency/100)) AS info
    FROM word
    WHERE
        (type = 'adjective' OR type = 'noun' OR type = 'verb') AND
        length(word) > 3 AND
        word NOT LIKE '%[^a-zA-ZäöüÄÖÜß]%' AND
        word NOT LIKE '%@%'
);